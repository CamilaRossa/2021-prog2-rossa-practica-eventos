﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using logica;
using static logica.Computadora;

namespace _2021_prog2_rossa_practica_eventos
{
    public static class ExtensionComputadora
    {
        public static bool ValidarMemoria(this Computadora computadora, string memoriaIngresada)
        {
            bool result = Enum.GetNames(typeof(ECantidadMemoria)).Contains(memoriaIngresada);
            return result;

        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Principal principal = new Principal();
            principal.itemMovimiento += movimientoItemHandlerEvent;
            var nombreProducto=Console.ReadLine();

            Console.WriteLine("¿Qué desea ingresar?");
            var rta = Console.ReadLine();
            if (rta=="Computadora")
            {  
                Computadora computadora = new Computadora();
                Console.WriteLine("Ingrese la descripción del procesador");
                computadora.DescripcionProcesador = Console.ReadLine();
                bool memoriaValida = false;
                while (memoriaValida==false)
                {
                    Console.WriteLine("Ingrese cantidad de memoria ram");
                    var x = Console.ReadLine();
                    memoriaValida = computadora.ValidarMemoria(x);
                    if (memoriaValida == true)
                    {
                        computadora.CantidadMemoriaRam = (ECantidadMemoria)Enum.Parse(typeof(ECantidadMemoria), x);
                    }
                }
                Console.WriteLine("Ingrese nombre fabricante");
                computadora.NombreFabricante = Console.ReadLine();
                principal.AgregarItem(computadora);
            }
            else
            {
                Monitor monitor = new Monitor();
                Console.WriteLine("Ingrese el año de fabricacion");
                monitor.AñoFabricacion = int.Parse(Console.ReadLine());
                Console.WriteLine("Ingrese cantidad de pulgadas");
                monitor.Pulgadas = int.Parse(Console.ReadLine());
                principal.AgregarItem(monitor);
            }
        }

        static void movimientoItemHandlerEvent(Object item, EventArgs e)
        {
            if (item is Monitor)
            {
                Monitor esMonitor = item as Monitor;
                Console.WriteLine($"Producto eliminado: { esMonitor.DevolverInfo()}");
            }
            else
            {
                Computadora esComputadora = item as Computadora;
                Console.WriteLine($"Producto eliminado: {esComputadora.DevolverInfo()}");
            }
        }
    }
}
