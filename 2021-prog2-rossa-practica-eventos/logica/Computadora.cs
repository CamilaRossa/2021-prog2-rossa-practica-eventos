﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace logica
{
    public class Computadora:Item
    {
        public string DescripcionProcesador { get; set; }
        public ECantidadMemoria CantidadMemoriaRam { get; set; }
        public string NombreFabricante { get; set; }

        public override string DevolverInfo()
        {
            return $"COMPUTADORA {Modelo} - {Marca} - {DescripcionProcesador} - {CantidadMemoriaRam} RAM - {NombreFabricante}";
        }

        public enum ECantidadMemoria
        {
            [Description("2GB")]
            twoGB,
            [Description("4GB")]
            fourGB,
            [Description("8GB")]
            eightGB,
            [Description("16GB")]
            sixteenGB,
        }

    }
}
