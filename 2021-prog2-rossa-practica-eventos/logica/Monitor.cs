﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace logica
{
    public class Monitor:Item
    {
        public int AñoFabricacion { get; set; }
        public bool EsNuevo { get { return AñoFabricacion == DateTime.Today.Year ? true : false; } }
        public int? Pulgadas { get; set; } //puede ser null

        public override string DevolverInfo()
        {
            return $"MONITOR {Marca} - {Modelo} - {Pulgadas}"; 
        }


    }
}
