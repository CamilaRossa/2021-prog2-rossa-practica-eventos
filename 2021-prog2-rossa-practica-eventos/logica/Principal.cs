﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace logica
{
    public class Principal
    {
        List<Item> ListaItem = new List<Item>();
        List<Monitor> ListaMonitor = new List<Monitor>();
        List<Computadora> ListaComputadora = new List<Computadora>();

        public EventHandler itemMovimiento;
        public void AgregarItem(Monitor monitor)
        {
            ListaItem.Add(monitor);
            itemMovimiento(monitor, new EventArgs());

        }
        public void AgregarItem (Computadora computadora)
        {
            ListaItem.Add(computadora);
            itemMovimiento(computadora, new EventArgs());
        }

        public void EliminarItem(string id_eliminar)
        {
            var elementoAEliminar = ListaItem.FirstOrDefault(y => y.Identificador == id_eliminar);
            if (elementoAEliminar!=null)
            {
                ListaItem.Remove(elementoAEliminar);
            }
            else
            {

            }
        }

        public List<Item> obtenerLista()
        {
            List<Item> ListaNueva = new List<Item>();

            ListaNueva.AddRange(ListaMonitor);
            ListaNueva.AddRange(ListaComputadora);

            return ListaNueva.OrderBy(x => x is Computadora).ToList();
        }
    }
}
