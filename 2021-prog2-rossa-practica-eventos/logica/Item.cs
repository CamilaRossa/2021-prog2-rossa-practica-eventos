﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace logica
{
    public abstract class Item
    {
        public string Modelo { get; set; }
        public string Marca { get; set; }
        public int NumeroSerie { get; set; }
        public string Identificador { get { return (Modelo + Marca + NumeroSerie); } } //propiedad solo lectura

        public abstract string DevolverInfo();
    }
}
